import click
from rc3_tools.configuration import Configuration
from rc3_tools.map.map_loader import MapLoader

config = Configuration()


@click.option(
    "--username",
    "-u",
    type=str
)
@click.option(
    "--password",
    "-p",
    type=str
)
@click.group()
def cli(username: str, password: str) -> None:
    if username or password:
        global config
        config = Configuration(username=username, password=password)


@cli.command()
def map() -> None:
    loader = MapLoader(config=config)
    loader.run()
