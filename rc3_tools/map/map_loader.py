from rc3_tools.configuration import Configuration

from selenium.webdriver.firefox.options import Options as FFOptions
from seleniumwire import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
import time
import requests
import json


class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


class MapLoader:
    config: Configuration
    browser: webdriver.Firefox
    map_queue: list
    map_done: dict[str, set]

    def __init__(self, config: Configuration):
        self.config = config
        ffoptions = FFOptions()
        # ffoptions.add_argument('--headless')
        self.browser = webdriver.Firefox(options=ffoptions)
        self.browser.implicitly_wait(3)
        self.map_queue = []
        self.map_done = {}

    def run(self):
        self.login()
        self.enterworld()
        self.map_queue.append(self.browser.current_url)
        self.handle_map()
        with open('data/maps.json', 'w') as f:
            f.truncate()
            f.write(json.dumps(self.map_done, cls=SetEncoder, indent=2))
        self.browser.close()

    def enterworld(self):
        worldbtn: WebElement = self.browser.find_element(By.XPATH,
                                                         '/html/body/div[6]/header/nav/a[1]')
        worldbtn.click()
        time.sleep(2)
        enterbtn: WebElement = self.browser.find_element(By.XPATH,
                                                         '/html/body/div[6]/main/article/div/a')
        enterbtn.click()
        current_tab = self.browser.current_window_handle
        tabs = self.browser.window_handles
        for tab in tabs:
            if tab != current_tab:
                self.browser.switch_to.window(tab)

        time.sleep(5)

        charbtn: WebElement = self.browser.find_element(By.XPATH,
                                                        '/html/body/div[1]/div[1]/div[2]/div/div/form/section[2]/button[1]')
        charbtn.click()
        time.sleep(2)
        vidbtn: WebElement = self.browser.find_element(By.XPATH,
                                                       '/html/body/div[1]/div[1]/div[2]/div/div/form/section[3]/button')
        vidbtn.click()

    def login(self):
        self.browser.get(self.config.login_url)
        namefield: WebElement = self.browser.find_element(By.XPATH,
                                                          '//*[@id="id_username"]')
        namefield.send_keys(self.config.username)
        passfield: WebElement = self.browser.find_element(By.XPATH,
                                                          '//*[@id="id_password"]')
        passfield.send_keys(self.config.password)
        btn: WebElement = self.browser.find_element(By.XPATH,
                                                    '/html/body/div[6]/article/form/div/ul/li[3]/button')
        btn.click()
        time.sleep(2)

    def handle_map(self):
        if len(self.map_queue) == 0:
            return
        map_url = self.map_queue.pop(0)
        if map_url in self.map_done:
            return

        print(f"handling {map_url}")

        self.map_done[map_url] = set()

        map_json_url = ''
        self.browser.get(map_url)
        for request in self.browser.requests:
            if request.response and request.url.endswith(
                    '.json') and 'maps' in request.url:
                map_json_url = request.url
        del self.browser.requests

        r = requests.get(map_json_url)
        exits = self.get_exists(map_json=r.json())
        for exit in exits:
            if '/@/' in exit:
                nextmap_url = self.config.base_url + exit
            else:
                nextmap_url = map_url.rsplit('/', 1)[0] + '/' + exit

            nextmap_url = nextmap_url.split('#')[0]
            self.map_done[map_url].add(nextmap_url)

            if nextmap_url in self.map_done:
                continue
            if nextmap_url not in self.map_queue:
                self.map_queue.append(nextmap_url)

        self.browser.close()
        self.handle_map()

    def get_exists(self, map_json: dict) -> list:
        exits = []
        for layer in map_json['layers']:
            if 'properties' in layer:
                for property in layer['properties']:
                    if property['name'] == 'exitUrl' or property[
                        'name'] == 'exitSceneUrl':
                        exits.append(property['value'])
        return exits
