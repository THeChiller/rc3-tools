from __future__ import annotations

from pydantic import BaseSettings


class Configuration(BaseSettings):
    username: str = ''
    password: str = ''
    login_url: str = 'https://rc3.world/2021/login'
    base_url: str = 'https://visit.rc3.world'
